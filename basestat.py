#Jin Hoon Jung - module 4 Baseball Stats Counter

import re

import sys, os

playeroster = []
battingavg = []
 
if len(sys.argv) != 2:
	sys.exit("Usage: %s filename" % sys.argv[0]) #Usage Message

filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(sys.argv[1], "r")
for line in f:
    stName = re.match('^(([A-Z]{1}[a-z]+)+(\s[\w\-]+))', line.rstrip())
    if stName is not None:
        striN = stName.group()
        if striN not in playeroster:
            playeroster.append(striN)
f.close

for pName in playeroster:
    f = open (filename, "r")
    batsnum = 0.0
    hitsnum = 0.0
    for line in f:
        fullN = re.match('^(([A-Z]{1}[a-z]+)+(\s[\w\-]+))', line.rstrip())
        if fullN is not None:
            fName = fullN.group()
            if fName == pName:
                stats = re.findall(r'\d+', line.rstrip()) #find and store one or more (all) occurence of integers in the line read
                bat = float(stats[0])
                hit = float(stats[1])
                batsnum = batsnum + bat
                hitsnum = hitsnum + hit
    bat_avg = "%.3f" % ((hitsnum)/(batsnum)) #round up to thousandth decimal place
    f.close
    battingavg.append(bat_avg)

pands = dict(zip(playeroster, battingavg)) #create dictionary where playername:battingaverage
pands = pands.items() #create an array of the dictionary pairs
pands = sorted(pands, key = lambda x: float(x[1]), reverse = True) #sorts array by batting average, from highest to lowest average

for i in range (0,len(playeroster)):
    print pands [i][0] + ": " + pands[i][1]